package scratches.tc.postgres.book.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;

import static lombok.AccessLevel.PROTECTED;

/**
 * @author Rashidi Zin
 */
@Embeddable
@AllArgsConstructor
@NoArgsConstructor(access = PROTECTED)
public class Author {

    @Getter @Setter
    private String name;

}
