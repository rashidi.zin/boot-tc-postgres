package scratches.tc.postgres.book.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import static javax.persistence.GenerationType.TABLE;
import static lombok.AccessLevel.PROTECTED;

/**
 * @author Rashidi Zin
 */
@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor(access = PROTECTED)
public class Book {

    @Id
    @GeneratedValue(strategy = TABLE)
    private Long id;

    @NonNull
    private Long isbn;

    @NonNull
    private Author author;

    @NonNull
    private String title;

}
